#!/usr/bin/env python3
import time, os
from datetime import datetime
from queue import Queue
import threading
from time import mktime
from fuzzywuzzy import process
import schedule
from staticmap import StaticMap, Polygon, CircleMarker
import requests
from xml.etree import ElementTree as ET
import feedparser
import re
import random
from mastodon import Mastodon
from bs4 import BeautifulSoup

url = "http://www.rfs.nsw.gov.au/feeds/majorIncidents.xml"

mastodon = Mastodon(
    client_id=os.environ.get("MASTODON_CLIENT_ID"),
    client_secret=os.environ.get("MASTODON_CLIENT_SECRET"),
    access_token=os.environ.get("MASTODON_ACCESS_TOKEN"),
    api_base_url=os.environ.get("MASTODON_API_BASE_URL"))


def get_fire_image(where):
    success = False
    max_zoom = 17

    while not success:
        if max_zoom < 10:
            print("ZOOM LEVEL less than 10! WTF")
            break
        m = StaticMap(1280, 720, 150, 150, "http://c.tile.stamen.com/watercolor/{z}/{x}/{y}.jpg",
            max_zoom=max_zoom,
            tile_request_timeout=30)
        if where['type'] == "Point":
            marker = CircleMarker(where['coordinates'], "black", 20)
            m.add_marker(marker)
        elif where['type'] == "Polygon":
            for polyc in where['coordinates']:
                poly = Polygon(polyc, "black", "red")
                m.add_polygon(poly)
        try:
            image = m.render()
            success = True
        except Exception as e:
            print("Couldnt render, reducing zoom")
            print(e)
            max_zoom = max_zoom - 1
            continue
        image.save("image.jpeg")
        return
    raise ValueError("Couldn't render map.")


def search_get_fire_image(search_text):
    feed = feedparser.parse(url)
    entries = list(feed.entries)

    if "random" in search_text.lower() or len(search_text.strip()) == 0:
        e = random.choice(entries)
        title = e['title']
        where = e['where']
        category = e['category']
        when = datetime.fromtimestamp(mktime(e['published_parsed']))
        get_fire_image(where)
        return title, category, when

    titles = [x['title'].lower() for x in entries]
    results = process.extractOne(search_text.lower(), titles)
    thetitle = results[0]

    for i,e in enumerate(entries):
        title = e['title']
        if title.lower() != thetitle.lower():
            continue
        category = e['category']

        where = e['where']
        when = datetime.fromtimestamp(mktime(e['published_parsed']))
        print(f"rendering fire {title.title()}")
        
        get_fire_image(where)
        return title, category, when
        



def reply_to_status(status):
    text = status['content']
    
    soup = BeautifulSoup(text, 'html.parser')

    spans_to_delete = soup.find_all('span')
    for span in spans_to_delete: 
        span.extract()
    text = str(soup)

    text = text.replace("<p>","").replace("</p>","").strip().lower()
    try:
        title, category, when = search_get_fire_image(text)
    except Exception as e:
        mastodon.status_reply(status, 
            f"I had an error:\n{str(e)}")
        print(f"error {str(e)}")
        return

    print(f"tooting reply {title}...")
    mtoot = mastodon.media_post("image.jpeg", 
        mime_type="image/jpeg", 
        description=f"{title.title()}\nMap by stamen.org CC-by-sa")
    whenf = when.strftime("%Y-%m-%d %H:%M")
    toot = mastodon.status_reply(status,
        f"{title.title()} - {category} at {whenf}",
        media_ids=[mtoot['id']])

def run_mentions():
    try:
        notifications = [x for x in mastodon.notifications() if x['type'] == "mention"]
        
        for notification in notifications:
            reply_to_status(notification['status'])
            mastodon.notifications_dismiss(notification['id'])

    except Exception as e:
        print(f"error {str(e)}")



def run_random():

    feed = feedparser.parse(url)
    entries = list(feed.entries)
    e = random.choice(entries)
    
    title = e['title']
    where = e['where']
    category = e['category']
    when = datetime.fromtimestamp(mktime(e['published_parsed']))

    try:
        print(f"rendering fire {title.title()}")
        get_fire_image(where)
        mtoot = mastodon.media_post("image.jpeg", mime_type="image/jpeg", 
            description=f"{title.title()}\nMap by stamen.org CC-by-sa")
        whenf = when.strftime("%Y-%m-%d %H:%M")
        toot = mastodon.status_post(f"{title.title()} at {whenf}", 
            visibility="unlisted",
            media_ids=[mtoot['id']])
    except Exception as e:
        print(f"error {str(e)}")

def worker_main():
    while True:
        job_func = jobqueue.get()
        job_func()
        jobqueue.task_done()

jobqueue = Queue()

def run_threaded(job_func):
    job_thread = threading.Thread(target=job_func)
    job_thread.start()

schedule.every(1).second.do(jobqueue.put, run_mentions)
schedule.every().hour.do(run_threaded, run_random)

print("starting worker thread")

worker_thread = threading.Thread(target=worker_main)
worker_thread.start()

print("entering runloop")
while True:
    schedule.run_pending()
    time.sleep(1)