FROM alpine


RUN apk add --no-cache py3-pip py3-pillow py3-requests py3-future

RUN pip3 install --no-cache-dir Mastodon.py schedule fuzzywuzzy bs4


ENV MASTODON_CLIENT_ID ""
ENV MASTODON_CLIENT_SECRET ""
ENV MASTODON_ACCESS_TOKEN ""
ENV MASTODON_API_BASE_URL ""
ENV PYTHONUNBUFFERED=1

WORKDIR /usr/src/app

COPY . .

RUN cd feedparser && \
    python3 setup.py install
RUN cd staticmap && \
    python3 setup.py install

CMD ["python3", "/usr/src/app/run.py"]